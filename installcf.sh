#!/bin/bash
ARCH="i386"
if [ "$(/bin/uname -m)" == "x86_64" ]; then
    ARCH="amd64"
fi
DEBFILE="cfengine-community_3.3.8_${ARCH}.deb"
wget "https://bitbucket.org/powant/public/downloads/${DEBFILE}" -O /tmp/${DEBFILE}
dpkg -i /tmp/${DEBFILE}

# tags
mkdir -p /var/cfengine/inputs /etc/default/tags
touch /etc/default/tags/$(hostname | sed -e 's/[0-9]//g' | cut -d. -f1)

# failsafe
wget "https://bitbucket.org/powant/public/get/master.tar.gz" -O - 2>/dev/null | tar zxvf - -C /var/cfengine/inputs/ --strip=1 --wildcards */failsafe.cf
echo "" >>/var/cfengine/inputs/failsafe.cf # dispist datetime
chmod 0600 /var/cfengine/inputs/failsafe.cf

# timezone
cp -pf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

# try to run ntp
apt-get -y install ntpdate
/etc/init.d/ntp stop 2>/dev/null
ntpdate -s cf.powant.com

# cfengine
/var/cfengine/bin/cf-key
/var/cfengine/bin/cf-agent -KI -f /var/cfengine/inputs/failsafe.cf
/etc/init.d/cfengine3 restart
/var/cfengine/bin/cf-agent -KI
/var/cfengine/bin/cf-agent -KI
